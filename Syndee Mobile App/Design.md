# Design
This page contains all the resources for UX/UI design of Syndee Mobile App

## UX and Market Research Documents

Competitor Research: https://miro.com/app/board/o9J_l2M9vzM=/?moveToWidget=3074457362549640331&cot=14

User interviews are  in our DoveTail account. **Ask Maria for access**
Analysis and insights from user interviews: 
**https://miro.com/app/board/o9J_l2M9vzM=/?moveToWidget=3074457365471354923&cot=14**

## UX/UI Design

Lei Lei's initial designs: https://www.figma.com/file/g3yMDTLAxsXNANvLtRm1TV/Syndee!?node-id=0%3A1

Springboard team designs: https://www.figma.com/file/TwS3fC4q4CsC2BdACkOXkz/Tangible-AI---Springboard-Team-Project?node-id=0%3A1

"Production version" design: https://www.figma.com/file/oiOQeOYnJxzcVqkcjOgUOM/Syndee-Mobile-App-Design?node-id=56%3A116
