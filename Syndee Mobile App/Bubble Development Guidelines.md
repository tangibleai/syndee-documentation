# Bubble Development Guidelines

# Architecture

* Syndee is a single-page app - all the screens are incorporated into the index page.
* Every screen (except the signup one) is in its own **Reusable element**. This helps to keep the workflows organized 
* All screen names are part of Option Set screen names. 
* To support back-navigation, whenever the user chooses to switch a screen, the app should Navigate to a new page, while passing the screen name as a url parameter.

# Naming

1. All the names in the app should be in English - please check for typos and grammar mistakes!

2. Visual Element naming:
* Every Group name should start with the type of the group
*For example FloatingGroup Congratulations, Group Journal Screen*   
* Every Element on a screen should have its name start with the name of an element, followed by a name of the screen, a dash, and the name of the element. 
*For example "Button Journal - Talk to Syndee"*

3. Data Objects naming:
* Every Data Type name should be camel-case without spaces. *For example: JournalEntry*
* All the fields in data objects should be lower case, separated by underdashes. *For example: entry_type*

# Responsive Design

We want Syndee to naturally fit most mobile screens. 

The smallest phone we're currently designing for is iPhone S, which is 320x480px.
Here's a good manual for iPhone screens: https://www.paintcodeapp.com/news/ultimate-guide-to-iphone-resolutions  