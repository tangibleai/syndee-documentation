# Product

User stories repository (including votes): https://docs.google.com/spreadsheets/d/1efHzNU-J1eAhFkXYnukN-Umt5FwXiHi4QXiZCDx4rso/edit?usp=sharing


Product specification: https://docs.google.com/document/d/1dMCUS3Vx2R9iNpugPOlYfZKV1qUHyBflywyhJbvXqEM/edit

Current Sprint (Sprint 1): https://app.clickup.com/10513242/v/li/174017388

## Product Roadmap  (based on feature prioritization):

MVP v1 (design finished and implemented here: https://www.figma.com/file/oiOQeOYnJxzcVqkcjOgUOM/Syndee-Mobile-App-Design?node-id=56%3A116):

* login + signup screen
* chatbot screen + checkin flow (https://drive.google.com/file/d/1s0mszn7h8SPOqWsdFfbQUk8yJ8o_OnDY/view?usp=sharing) 
* user can journal a thought 
* user can see past affirmations and journaled thoughts on a journal screen 
* user can report feedback
* basic dashboard screen 
* goal setting 

MVP v3 - will be used during the pilot in CMC:

* push notifications 
* assessment Pre and Post 
* polish everything and prepare for launch