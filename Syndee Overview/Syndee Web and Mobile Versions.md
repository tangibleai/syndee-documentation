# Syndee Web and Mobile Versions

Syndee project centers around two main version: the **web demo** and the **mobile app**.

**The web demo** is a free tool designed to educate users about impostor syndrome and let them experience Syndee's interactive exercises. 

**The mobile app** is a tool that is meant to help user create a sustained progress in coping with self-sabotaging thinking. It is available on iOS and Android, and includes several features in addition to the chatbot. 