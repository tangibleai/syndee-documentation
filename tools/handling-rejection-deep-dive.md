# Handling Rejection Deep Dive 
*Situations*: Failure, challenge, rejection, hardship
*Pillars*: Examine, act more

## Background 
When dealing with rejection, it might be beneficial to look for patterns that could aid the user in indentifying what is primarily bothering them. Indentifying these thought patterns could both aid the user alleviate their stress and also help them acclimate to what their usual process is in handling rejection while providing them with skills that might help make the process much more efficient. These exercises are meant to help the user identify what their thought pattern is, similar to the existing "deep dive" tool, but tailored toward handling rejection.

## Exercise Tasks
1. Thanks for sharing this experience with me. Rejection is something inevitable. Know that everyone experiences this throughout their life and we all have to learn how to deal with it.

Sometimes, rejection can be seen through patterns and/ or similarities. These patterns below are some of the most common, select which one relates to your situation the most to find helpful insight on how to combat them.

- **Taking it personally**
    - Ex: "This outcome proves/ is evidence that I'm not fit for this"
    - To combat this, we can remind ourselves that rejection is normal and look at public figures/ celebrities to normalize rejection.

- **Making predictions**
    - Ex: "I will never be successfull"
    - To combat this, start making plans to look for the next opportunity that's arising.

- **Comparing yourself to others**
    - Ex: "I am not good enough at this task than they are"
    - To combat this, we can remind yourself that there is no right or wrong path to follow in life, everyrone takes a different route. Begin making plans to see what you can do to improve your chances for the future.

- **Feeling that you aren't fit for the role**
    - Ex: "I won't be able to perform this role, I won't be good at it"
    - To combat this, see what can be done to improve your skills, and remind yourself that there is always room for improvement, but it's important to try and perform the role to the best of your ability.

2. (After providing the user with the thought patterns, the user will select their choice, to which an example and a way to combat the thought will show.)

I hope these patterns and way to combat them were of help. Would you like to continue our deep dive into handling rejection?

3. Set the scene for what happened that led to your rejection. What specific feelings are coming to mind?

4. Let's look at these thoughts regarding your rejection and separate **facts** from **beliefs**. If this is a belief, is it accurate? If not, create **affirmation** with the accurate version of the statement. 

3. Will this rejection be permanent? What can you start doing to prepare for future interviews? Create an **action statement**  for your commitment. 

4. Ask the user if the situation is pervasive (does it occurs in other areas of their life). If not, create a thought that highlights the positives together with the negatives. 

## Additional Reading
https://lateralaction.com/wp-content/21C-resources/21C%2022%20Rejection.pdf, page 4
https://www.science.org/content/article/how-handle-rejection-your-professional-life