# Clarity Through Journalism
*Situations*: Confusion, hardship, nervousness
*Pillars*: Clarity, self-care

## Background 
When thinking about your goals and future, the direction in which your heading at can be unclear at times. Before diving into other tools or activities, sometimes it's important to understand the situation you're in right now and evaluate the path(s) you want to take. Through (strength) journaling, an activity related to positive psychology, you can start to understand and be aware of...

- What is your why?
- What are your goals?
- What is your plan?

**Journaling**: a record of experiences, ideas, or reflections kept regularly for private use. 

## Exercise Tasks
1. Thanks for sharing with me. It's great to start your day with some journaling, it can help identify the direction in which you're heading at and where you want to go with your goals and overall mindset.

 Let's start  easy: What's something that went well today?

2. Now, keep thinking about this moment and ask yourself: What did I do to contribute to this positive experience?

3. An important part of journaling is also reflecting on the negative and seeing what you can take from it. 

 Let's switch gears: What's something that didn't go as planned today?

4. Thanks for sharirng. Reflect on this: What can I do to change this experience in the future?

5. Let's dive deeper, what's something internal that you plan to work on in the future, for yourself?

6. Think back to the past, have the goals you've previously set still reflect and align with each other? Have you worked efficiently on these goals?

## Additional Reading
- https://drive.google.com/file/d/1-fp5m3RsbGPHV5heVJQU6ttfHY8WKzgz/view?usp=sharing
- https://www.ed.ac.uk/reflection/reflectors-toolkit/goals-objectives-habits

## Examples of Questions (taken from second link)
- What were 3 things that went well today/this week? How do you know?
- What was a situation today/this week where I could have done better? How?
- What was your biggest challenge today/this week? How did you overcome it?
- What was the predominant feeling you had today/this week? Why?
- What made you happy/sad/frustrated/angry/etc today/this week? Can you find some way of having more or less of the identified aspects?

- Am I optimising my time, energy and performance according to my values, goals and objectives?
- Am I making the most of opportunities available to me?  
- Am I working effectively within any fixed restrictions?  
- Where there are barriers, am I identifying them and tackling or circumventing them where possible?
- Do my values, goals and objectives still align with each other? Is this reflected in how I am spending my time?
- Are my goals still the right ones to deliver on my values?  
- Should/Can I refine or revise the strategies I am using for fulfilling my values and goals?