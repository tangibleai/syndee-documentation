# Stop the Blame
*Situations*: Failure, self-doubt, nervousness, negative consequences 
*Pillars*: Examine

## Background 
Internalizing blame and negative thoughts when dealing with an unintentional occurance, outcome, or situation is inherently common. The concept of **self-blame** is often referred to as believing you are the main cause of an outcome or disagreement, and having to take responsibility for the situation itself. These exercises aim to help the user dismantle their self-blame.

## Exercise Tasks
1. Work on distinguishing self-blame and self-criticism. What can be attributed as taking responsibility for actions, and what can be seen as learning experiences where there could be room for growth in the future?

2. Analyze the various factors that contributed to your performance/ situation. Instead of pinning the entire situation onto yourself, what other factors could've led up to, or had an impact on, this situation?

3. Pinpoint aspects that you were directly and explicitly responsible for. Approach these aspects in a non-judgmental and compassionate manner, be kind to yourself when describing these.

4. How can you try and be more consciously aware of whenever you're blaming yourself to an extreme? Create an **action plan** for future use.

## Literature/Scientific Sources 
Neff, Kristen D., Ya-Ping Hsieh, and Kullaya Dejitterat, “Self-Compassion, Achievement Gals, and Coping with Academic Failure,” Self and Identity (2005), 4, 263-287

## Sources
https://www.psychologytoday.com/us/blog/tech-support/201801/tackling-self-blame-and-self-criticism-5-strategies-try
