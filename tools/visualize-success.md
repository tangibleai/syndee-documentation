# Visualize Success 
*Situations*: 
*Pillars*: Examine, celebrate, act more

## Background 

This activity allows the user to visualize success in a third-person view, aiding in motivating the user.

## Exercise Tasks
1. Thanks for sharing with me. Sometimes it's hard to see the light at the end of the tunnel amidst a cloud of tasks and hardships. Let's try to view this situation as an outsider.
 

3. Gradually visualize your desired outcome. For example, if you're nervous about a presentation coming up, picture yourself successfully doing the presentation and recieving positive critiques. 

- Set the scene first. Where is this event happening? 
- What tasks need to be completed?
- What did you do to complete these tasks?
- What was the result?

4. You can repeat this process a few times with different scenarios. How can you apply this for future events?

5. Create an **action plan** for the steps you can take to start **visualizing success** from the third-person.

*Syndee can make these steps into something printable/ downloadable.*

Example: 

**Steps to Visualize Success**
1. Set the scene. View the location from the outside/ fly on the wall.
2. Visualize yourself in the area.
3. Acknowledge the tasks that need to be completed.
4. Imagine yourself completing the tasks sucesfully.
5. View the result and apply it to the real-world.

## Additional Reading
  Vasquez, Noelia A., and Roger Buehler. “Seeing Future Success: Does Imagery Perspective Influence Achievement Motivation?” Personality and Social Psychology Bulletin, vol. 33, no. 10, Oct. 2007, pp. 1392–1405, doi:10.1177/0146167207304541. (https://journals.sagepub.com/doi/abs/10.1177/0146167207304541)
