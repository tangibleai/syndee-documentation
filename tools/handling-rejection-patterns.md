# Handling Rejection Patterns

## Background 
It's incredibly important to prepare for the worst, especially when hoping for the best. Rejection is  very common, and something that is inevitable as you navigate along academia, career paths, among other things. These exercises focus on helping a user deal with rejection and criticism, presenting various means of handling rejection altogether.

**Rejection**: The concept of declining, dismissing, turning down, or refusing a proposal/ offer.

## Exercise Tasks
1. Thanks for sharing your experience with me. Rejection is inevitable, and something that everyone experiences throughout their life. 

Let's see if we can rate how this rejection makes you feel. Allowing yourself to absorb these emotions and let them sit is very impactful. On a scale of 1-10, with one being the best and ten being the worst, how does this rejection feel?

2. Thanks for sharing this with me. Your feelings regarding your rejection are incredibly valid.

Why don't we try using a retrospective point of view to guide us with coping with rejection. Think back to an earlier rejection, whether it be in academia or in a career field, on a scale of 1-10 how did that rejection feel?

3. Analyze both of your answers. Do you notice any progress? Does it seem like your thoughts/ feelings regarding rejection seem to be getting better over time? If yes, it means you're allowing yourself to understand how rejection is part of the process towards achieving ultimate success.

4. Learning from others can also be useful in helping normalize the idea of rejection. Here's a few celebrities or public figures that have dealt with rejection, yet have achieved lots of success.

- Jennifer Aniston
- Steve Jobs
- Oprah Winfrey
- Madonna
- Walt Disney
- Lady Gaga
- Anna Wintour

- https://www.heart.co.uk/showbiz/10-stars-who-were-rejected-before-making-it-big/
- https://thoughtcatalog.com/rachel-hodin/2013/10/35-famous-people-who-were-painfully-rejected-before-making-it-big/

[Flash cards with quotes from celebrities, public figures can be presented and given in a pdf format for users to print.]

5. Let's create an **action statement** to remind ourselves that rejection is completely normal, and something that everyone experiences.

Example: "Rejection is something that everyone experiences, and part of the road to success..."

6. [After creating the action statement, Syndee can present self-care resources already implemented in the app.]

## Literature 

## Additional Reading
https://lateralaction.com/wp-content/21C-resources/21C%2022%20Rejection.pdf, page 4
https://www.science.org/content/article/how-handle-rejection-your-professional-life
