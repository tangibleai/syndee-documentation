# Reframing (Your) Negative Self-Talk
*Situations*: Failure, challenge, nervousness, scared, anxious
*Pillars*: Examine, act more

## Background 
The neverending stream of thoughts that direct your actions and feelings refers to your **self-talk**. These thoughts are both positive and negative at times, and can easily get overwhelming when dealing with feelings towards failure, challenges, self-worth, and confidence. These exercises help the user reframe their internal dialogue. 

## Exercise Tasks
1. Thanks for being vulnerable with me. [Introduce the definition of what your self-talk is.] In the situation you described, what would you say your self-talk is referring to?

2. Is this simply a belief? Would you say there is any evidence to support these thoughts? (If there is no evidence to support these thoughts, continue to reframing. If the user indicates there is evidence, point them toward more concrete mental health/ self care resources, such as reaching out to someone (friends, family, mental health experts) that can be of help with their specific situation.)

3. Let's try and look at this situation from another perspective. How can we view this in a more positive light?

4. Do you think you learned any new skills from this situation? Does this affect your outlook on future situations?

5. Let's say you find your internal dialogue skewing towards a negative perspective in the near future. What can you tell yourself to stop this?

6. Based on your answer, create an action plan that can help you approach challenging situations in a more positive light.

Action Plan Example: "Next time I find myself dealing with [insert situation], or something similar, I'll remind myself that it's simply my inner thoughts thinking negatively and I should try looking at this in a positive light."

## Sources
[Techniques for reframing self talk](https://www.saintbelford.com.au/blogs/blog/9-powerful-techniques-for-reframing-your-self-talk)