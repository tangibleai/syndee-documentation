# Pep Talk Autonomy

## Background 
A **pep talk** is a **short speech** designed to help you feel more courageous or productive or enthusiastic. Usually, we can give the best pep talks to ourselves because we know our own minds — including our strengths and shortcomings — better than anyone else. One of the proven ways for a good pep talk is to emphasize your autonomy and choice. 

## Exercise Tasks
1. Thanks for being vulnerable with me. Your feelings are completely valid. Sometimes all we need is to analyze our situation, reframe our goals, and give ourselves a pep-talk. [Introduce the definition of pep-talk.]

2. When looking at your situation, it's important to understand how you have completely autonomy over your circumstances. You have the power to control your choices and make your own decisions. Let's go over your goals first.

3. While being ambitious can be great at times, you have the power to pursue goals that can motivate you rather than feel controlled by. Are there any goals that you think could adjust or tailor in a more realistic manner? 

4. Great, thanks for sharing! Sometimes, it's helpful to cut your goal in half and make it more digestible!

5. Now that we've gone over your goals and have tailored them when necessary, let's try and focus our thoughts on positivity through a pep talk!

6. Try speaking to yourself in third-person, remind yourself of your newly tailored goals and try to talk through them out loud. 

7. This doesn't have to be a speech, just a couple of words or sentences filled with positive affirmations works perfectly. Try it once more with these thoughts in mind.

8. Try to work through this regularly, find time during your week to readjust your goals and give yourself a pep talk.

## Literature
https://pubmed.ncbi.nlm.nih.gov/29154635/

## Additional reading
https://www.visualthesaurus.com/cm/wc/how-to-give-yourself-a-pep-talk-that-actually-works/
