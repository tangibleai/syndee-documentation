# Third Person Perspective
*Situations*: Challenge, anxiety, nervousness
*Pillars*: Examine, act more

## Background
When facing a negative situation, hardship, or challenge, it's incredibly easy to **ruminate** on the issue itself, leading to negative thoughts that contribute to self-down or feelings of hopelessness and insignificance. These exercises are meant to help the user look at their issues through another lens or in a "third-person" view, allowing the user to gain a new perspective on the issue.

**Ruminating** is the act of continuously pondering on a thought, usually negative, and repeating this process, establishing it as a never-ending cycle.

## Exercise Tasks
1. Thanks for sharing your situation with me. [Insert definition of ruminating.] Have you found yourself ruminating about this event? [If so, Syndee could provide brief support in terms of what ruminating is and when it can become excessive, can be tied back to self-care.]

2. Now, think of the event as if you were talking about your friend. Describe the event out loud, or in your head, and substitute your pronouns for your own name.

For example, "Why does {Name} feel this way?" Separate yourself from the event and try to see yourself as an observer.

Take a couple of minutes to do this. [Syndee can provide a timer.]

3. Now that you can view the situation as an outsider, what thoughts are coming to mind? 

4. Imagine your friend shares this situation with you and says: {Thought}. What would you tell {Name}?

5. Is viewing the situation as an outsider helping to ease the stress? 

6. If so, create an **action plan** that can help you view the situation through the third-person.
Or: user input in part 4 as an affirmation.

Example Action Plan: 

"Whenever I find myself ruminating about a situation such as when I [Insert situation discussed], I can make sure to think of the event as an outsider first."

## Literature/Scientific Sources 
Kross, E., Bruehlman-Senecal, E., Park, J., Burson, A., Dougherty, A., Shablack, H., Bremner, R., Moser, J., & Ayduk, O. (2014). Self-talk as a regulatory mechanism: How you do it matters. Journal of Personality and Social Psychology, 106, 304-324. 

https://ggia.berkeley.edu/practice/gaining_perspective_on_negative_events