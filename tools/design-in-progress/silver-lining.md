# Find a Silver Lining
*Situations*: Failure, challenge, negative consequences
*Pillars*: Examine

## Background 
Although we are all confronted with numerous hardships throughout our life, it's important to try and find the silver lining, a positive outlook, perspective, or thought, among the situation. These exercises provides the user with tips to aid them find the silver lining within their current situation.

## Exercise Tasks
1. Thanks for sharing with me. From what you've stated, would you say there is room to find a positive perspective within the situation? 
    * If yes, continue to finding the silver lining. 
    * If the situation is more permanent, provide them with mental health resources/ self care.)

2. Explain the concept of silver lining and ask the user if they're ready to apply it on their own thought. 

2. Ask the user if they'd like to try an activity where they can help someone find the positives in their situation.

Evaluate this statement.

"I failed to get investment."

"Although the presentation I delivered to investors didn't go as planned, I analyzed my preparation and the material I had and now know to how to prepare for future presentations and can strenghten my delivery."

Is there room to find a positive perspective within the situation?

3. Now, back to what you're dealing with. Regardless of any outcome, what are some of the good things you can take away frorm this experience?

4. Could this experience help you expand your skillset and grow? Try to find some positives!

5. Use the last positive statement as an affirmation


## Literature
Sergeant, S., & Mongrain, M. (2014). An online optimism intervention reduces depression in pessimistic individuals. Journal of Consulting and Clinical Psychology, 82(2), 263-274.

## Additional Reading
https://ggia.berkeley.edu/practice/finding_silver_linings
https://positivepsychology.com/find-a-silver-lining/

## Chatbot Design Link
https://docs.google.com/spreadsheets/d/1n5yl8h0UfbFkrThzCt3iZD_DaG6gkp5NVhgLQKa2nIk/edit?usp=sharing


