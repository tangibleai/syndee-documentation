# Identify Strength (Job Interview)
*Situations*: Job interview, doubt, nervousness, anxiety
*Pillars*: Examine, celebrate, act more

## Background 

It's important to thoroughly prepare for a job interview and know what exactly you bring to the table. These exercises are meant to aid the user with identifying their strength(s), whether it be a skill or quality, specifically before a job interview, providing the user with greater confidence overall and easing their nervousness.

## Exercise Tasks
1. I apprceciate your vulnerability. Interviewing for a job can sometimes be a little nerve-racking. In general, what skills, aspects, or qualities do you think you can bring to the table for any new job or position?

2. Now, let's apply these thoughts for this new job! What quality, skill, or aspect that you've previously mentioned do you think makes you a great candidate for this job? 

3. Let's continue preparing for this interview. Could you describe a situation where you had to use [insert skills mentioned previously]? *Beggining of STAR framework*

4. Describe the situation at-hand. What task needed to be done? Set the scene!

5. Could you explain what you did in response? How did you apply this skills?

6. What was the result? What did you learn from this?

7. Now, let's create an **affirmation statement** to remind yourself of your strenght(s) before your job interview!

"From my experience tackling [insert situation mentioned in activity], I applied [insert skill mentioned] which led me to [insert result]. This experience allowed me to recognize that this skill is a strength that I can bring to the table."

## Additional Reading