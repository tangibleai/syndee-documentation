# Growth Mindset Reframing
*Situations*: Failure, challenge
*Pillars*: Examine

## Background 

Those with a fixed mindset tend to stagnate, reinforcing the idea that they are "stuck" with the skills and capabilities that they currently have. However, those with a growth mindset tend to improve, constantly strengthening their skills. It's important to understand, adjust, and operate under a growth mindset consistently. This excercise allows one to reframe their thoughts and operate under a growth mindset.

**Growth Mindset**: The belief that we are in control of our skills. That they are things that can be developed and improved. That we have the capacity to learn and grow. Skills are built.

**Fixed Mindset**: The belief that our skills are set, that we have what we have, and that we cannot change them. That we don’t really have the capacity to change and learn. Skills are born.

## Sample Exercise Tasks
1. [Introduce the definitons of **Growth** and **Fixed Mindset** it's important for the user to be able to distinguish the two.]

2. Ask user if they want to participate in activity that can help distinguish a growth mindset from a fixed mindset.

Reflect on these statements: 

"I'm not good at economics, I struggle understanding the material and on exams. Some people are just born to process and apply the concepts." 

"While my understanding of these specific concepts in economics might not be the best yet, I'm willing to continue applying myself and studying to reach my goal in understanding the material."

Which statement is considered a **growth mindset**? Which statement is considered a **fixed mindset**? How can you tell?

3. Reflect on your thought/ statement and try to categorize it as having either a **growth** or **fixed** mindset. 


4. If your thought used a **growth mindset**, create an **affirmation** that displays your commitment to using a growth mindset in the future. -- (Syndee can stop here if user knows/ realizes what a growth mindset is and creates their action statement.)

Affirmation Example: "Through my experience in [insert situation], I've learned that [insert qualities/ skills user has learned] which help me recognize I can grow from my situation, hardships, and difficulties that I encounter, allowing me to operate under a growth mindset"

5. If your thought didn't use a growth mindset, how could you have applied it in this situation?

    * If you see your thought displaying fixed mindset, does that belong to one of the following [patterns](https://www.staples.com/Ashley-Growth-Mindset-Magnetic-Mini-BBS-12-x-17-ASAH77010/product_24063296)?

    * If you recognized one of the patterns, reframe the sentence using the pattern from the link.

    * If the user doesn't recognize any patters, challenge them to apply growth mindset to reframe the thought.

    * Use the reframed thought created above as an affirmation.

## Literature/Scientific Sources 
Carol S. Dweck, *Mindset: The New Psychology of Success* (summary https://fourminutebooks.com/mindset-summary/)

## Additional Reading and Inspiration
https://malcolmocean.com/2014/07/growth-mindset-reframing/
https://www.skillpacks.com/growth-mindset-activities

## Chatbot Design Link
https://docs.google.com/spreadsheets/d/1gEET_NQblhfokO4lLZz4XHWbHvZj_d5WX8hEZXR22tU/edit?usp=sharing
