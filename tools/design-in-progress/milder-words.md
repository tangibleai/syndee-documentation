# Milder Words

## Background

Have you ever been to a hospital and noticed how the nurses talk about ‘discomfort’ instead of ‘pain’? This is generally done because ‘pain’ is a much more powerful word, and discussing your ‘pain’ level can actually make your experience of it more intense than if you’re discussing your ‘discomfort’ level.

You can try this strategy in your daily life. In your self-talk, turning more powerful negative words to more neutral ones can actually help neutralize your experience.

Instead of using words like ‘hate’ and ‘angry’ (as in, “I _hate_ traffic! It makes me so _angry_!”), you can use words like ‘don’t like’ and ‘annoyed’ (“I don’t like traffic; it makes me annoyed,” sounds much milder, doesn’t it?)

## Exercise Tasks
1. Thanks for sharirng with me. Your situation and feelings are completely valid. Sometimes, our self-talk can "sabotage" us and our thoughts, leading to rumination and a frequent negative self-talk. It's important to attempt and view these negative thoughts and discussions in a much more positive light, reframing the experience entirely.

2. Let's try an activity! Instead of using harsh words such as "hate," "angry," and "mad," we can use **milder** words such as "annoyed" and "frustrated" to tailor these thoughts into becoming much milder.

3. For example: "I hate preparing for this job interview, I'm horrible at interviews and won't get the job."

4. This can be turned into a more "neutral" or "mild" thought, reframing it into: "I'm not looking forward to preparing for this job interview, it's not my strength, but I'll give it a shot."

5. Type out a short statement that can describe your situation, write it bluntly for now.

6. Now, let's try and use milder words to describe this statement. What would it look like in a more positive light?

7. Great! Now let's create an **action statement** to remind yourself to use **milder words** when discussing your not-so-positive experiences! We can add it to our **Goal Setting** menu. 

## Additional Reading
- https://www.edutopia.org/article/positive-words-go-long-way
- https://www.happify.com/hd/11-everyday-phrases-negative-to-positive/
- https://medium.com/jrni/the-6-steps-to-using-more-positive-language-4192da04c1c2

## Chatbot Link
https://docs.google.com/spreadsheets/d/1MSU6omCxy_VQCeemK_abMPM7yo58Yk0emQUw7vjzHnQ/edit?usp=sharing