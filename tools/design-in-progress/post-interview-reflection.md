# Post-Interview Reflection

## Background 
The nerve-wracking interview is finally over with! Now it's time to go over how the interview went to tryr to ease the post-interview nerves. It's important to analyze the interview afterward, regardless if the user thinks it went good or bad. These excercises aim to help the user analyze how their interview went in order to improve the user's interview skills and help them relax afterward.

## Exercise Tasks
1. I appreciate you sharing this with me. Congratulations on finishing your interview! Regardless of the outcome, you've accomplished something huge! Now, let's try to analyze how the interview went. 

What was this interview for? Was it for a specific position?

2. On a scale of 1-10, with ten being the best and one being the worst, how do you think the interview went?

3. Thanks for sharing. Let's see if we can get a bit more specific. Set the scene! Try to describe how the overall interview was.

4. Now that we've set the scene, we can start analyzing the details! Why do you believe the interview went the way it was?

5. Now, since your last interview, do you believe there was an improvement?

6. If yes, in what ways do you think you've improved? This can be in confidence, overall skills, presentation, etc. Briefly list some of the ways you think you've improved.

7. In what ways do you think you can improve for your next interview?

8. Let's create an action plan for future interviews! [This can be in the form of a list where the user describes some of the steps the user can take and then have them print it out in a pdf.]

- 1. 
- 2. 
- 3. 

## Literature 

## Additional Reading
https://www.etsy.com/listing/1164880466/interview-reflection-worksheet-printable
https://fortunaadmissions.com/tips-for-writing-the-hbs-post-interview-reflection/

## Chatbot Design Link
https://docs.google.com/spreadsheets/d/1IguC0hTTLriHn_VIdL5xJevybhwlqOTEJ6MSLj_7HJs/edit?usp=sharing