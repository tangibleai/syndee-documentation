# Relax Before a Job Interview
*Situations*: Stress, anxiety, nervousness
*Pillars*: Examine, act more, reach out

## Background 

It's incrredibly common to have nerves before going into a job interview. Although the user might've already identified their strenghts and have prepared, it's important to relax and calm your nerves before going into the interview. These activities will aid the user in relaxing before their job interview.

## Exercise Tasks
1. Thanks for sharing with me. It's completely normal to feel nervous before a job interview. Let's start with a short breathing excercise. These excercises are a great way to calm your nerves, and can even be used before the interview itself.

Use gif found at this link, or something similar accompanied with a two-minute timer. (https://images.app.goo.gl/CyFxwRo58TLVUkDr5)

2. It's important to feel confident going into the meeting, confidence does wonders for your performance. Give yourself a short pep-talk with some **affirmations**! What are some things you think you need to hear at the moment? 

For example: 
- I'm qualified for this. 
- I'm smart. 
- I believe in myself. 
- I am the ideal candidate
- I am determined to be sucessful. 

*Syndee can make these affirrmations into  note-cards that users can print and/ or save.*

3. Find a quiet place to do this, and say these affirmations out loud and with **confidence**.

4. Sometimes, it's also nice to reach out to your community for some motivation. The encouragement of a friend or family member can truly help. Think of someone you could reach out to and let them know what you're going through!

5. Lastly, remind yourself that an interview is always just a conversation. Identify your strenghts, prepare appropriately, and practice some self-care! Good luck!

## Additional Reading
Hardavella, Georgia et al. “How to prepare for an interview.” Breathe (Sheffield, England) vol. 12,3 (2016): e86-e90. doi:10.1183/20734735.013716 (https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5298161/)

https://www.themuse.com/advice/12-different-ways-to-calm-your-interview-nerves-because-youve-got-this

https://www.forbes.com/sites/jacquelynsmith/2013/03/26/14-tips-for-staying-calm-during-a-job-interview/?sh=70a0a18d5cec

https://www.fastcompany.com/3033706/want-to-be-successful-you-should-talk-to-yourself-more

## Chatbot Design Link
https://docs.google.com/spreadsheets/d/1O_Fm_KR9_ccmNSmnq0qtrdXGbckM2gkXqGdbgPwBK8w/edit?usp=sharing