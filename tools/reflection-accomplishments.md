# Reflection Through Accomplishments
*Situations*: Challenge, self-care, nervousness
*Pillars*: Examine, clarity, self-care

## Background 
It's important to monitor your progress towards a goal and revise it when needed. By describing your goals and amibitions and having a clear mindset on where you want to take this goal, as well as how you'll get there, you can properly reflect and grow.

## Exercise Tasks
1. Thanks for being vulnerable with me. It's important to try and outline your ambitions before taking them on, allowing yourself to head in the proper direction with a clear mindset. Something we can start off with is identifying a goal or ambition that you want to achieve! Write this down.

2. Rate where you currently are in achieving your goal or objective. A 0 indicates not started and 10 indicates completed. 

3. Great! If you rated yourself on a number greater than 0, list out 3 small steps that you've already done that contribute in achieving this goal or ambition, it doesn't matter how small they are!

4. Now, let's review or revise this current goal. Does it still reflect your values? Is this goal something that's achievable?

5. What are some strategies that have worked in the past for you to achieve your goals? Could you apply these for this goal?

6. It's important to reflect on your progress towards a goal, and celebrate whenever you can! What's something you can do to celebrate your goals?

## Additional Reading
https://drive.google.com/file/d/1rYuyAPkSe2utQgu4PC24dYIS-PCafmyV/view?usp=sharing