# Deep Dive (3P's)
*Situations*: Failure, challenge
*Pillars*: Examine, act more

## Background 
The language we use and the way we perceive our experiences — especially challenging ones — can have a lasting effect on who we become.
This exercise helps the user to reframe a statement using Martin Seligman's "Persistent, Permanent, Pervasive"

**Personalization** is thinking that the problem is yourself, instead of considering other outside things that have caused it. Realizing outside factors have caused a bad situation allows us to reduce the blame and criticism we put on ourselves.

**Permanence** is thinking a bad situation will last forever. Those who think setbacks are temporary have improved ability to accept and adapt for the future.

**Pervasiveness** is thinking a bad situation applies across all areas of your life, instead of only happening in one area. People who think bad situations are pervasive feel that all areas of their life are impacted. This can make it hard to carry on.

## Exercise Tasks
1. Look at your thought and separate **facts** from **beliefs**. If this is a belief, is it accurate? If not, create **affirmation** with the accurate version of the statement. 
1. If the situation is permanent. If the situation is not permanent, what you can do to change it? Create an **action statement**  for your commitment. 
1. Ask the user if the situation is pervasive (does it occurs in other areas of their life). If not, create a thought that highlights the positives together with the negatives. 

## Literature/Scientific Sources 
Martin E. Seligman, *Learned Optimism: How to Change Your Mind and Your Life*
JANE E. GILLHAM, ANDREW J. SHATTE, KAREN J. REIVICH, AND MARTIN E. P. SELIGMAN *Optimism, Pessimism and Explanatory Style* [link](https://psycnet.apa.org/record/2000-16270-003)

## Additional Reading
[Personal, Permanent, Pervasive](https://www.holstee.com/blogs/mindful-matter/personal-permanent-pervasive)
